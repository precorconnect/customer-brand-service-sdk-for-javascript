/**
 * @class {CustomerBrandSynopsisView}
 */
export default class CustomerBrandSynopsisView {

    _id:number;

    _name:string;

    /**
     * @param {number} id
     * @param {string} name
     */
    constructor(id:number,
                name:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

}
import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import DiContainer from './diContainer';
import Level1CustomerBrandView from './level1CustomerBrandView';
import Level2CustomerBrandView from './level2CustomerBrandView';
import ListLevel1CustomerBrandsWithSegmentIdFeature from './listLevel1CustomerBrandsWithSegmentIdFeature';
import ListLevel2CustomerBrandsWithBrandIdFeature from './listLevel2CustomerBrandsWithBrandIdFeature';
import GetCustomerBrandWithIdFeature from './getCustomerBrandWithIdFeature';
import CustomerBrandView from './customerBrandView';

/**
 * @class {CustomerBrandServiceSdk}
 */
export default class CustomerBrandServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {CustomerBrandServiceSdkConfig} config
     */
    constructor(config:CustomerBrandServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    getCustomerBrandWithId(customerBrandWithId:number,accessToken:string):CustomerBrandView{

        return this
            ._diContainer
            .get(GetCustomerBrandWithIdFeature)
            .execute(customerBrandWithId,accessToken);
    }

    listLevel1CustomerBrandsWithSegmentId(segmentId:number,accessToken:string):Array<Level1CustomerBrandView> {

        return this
            ._diContainer
            .get(ListLevel1CustomerBrandsWithSegmentIdFeature)
            .execute(segmentId,accessToken);

    }

    listLevel2CustomerBrandsWithBrandId(brandId:number,segmentId:number,accessToken:string):Array<Level2CustomerBrandView> {

        return this
            ._diContainer
            .get(ListLevel2CustomerBrandsWithBrandIdFeature)
            .execute(brandId,segmentId,accessToken);
    }

}
